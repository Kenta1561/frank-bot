package frank.bot.handlers.command

import discord4j.core.`object`.entity.channel.MessageChannel
import frank.api.Requester
import frank.api.response.BoardResponse
import frank.api.service.BoardService
import frank.commandPrefix
import frank.util.apiEmbedTemplate
import frank.util.getDateTimeString
import frank.util.isExtId
import retrofit2.Call

/**
 * Handles "departures" commands for showing upcoming departures from a station.
 */
class BoardHandler(private val boardService: BoardService) : CommandHandler<BoardResponse>() {

    /**
     * Creates an API requests from user-given arguments.
     */
    override fun createRequest(args: List<String>): Call<BoardResponse> {
        return boardService.getDepartures(args[2])
    }

    /**
     * Processes the received response from the API.
     */
    override fun processResponse(requester: Requester, channel: MessageChannel, response: BoardResponse) {
        channel.createMessage { msg -> msg.setEmbed(getEmbed(response)) }.subscribe()
    }

    /**
     * Creates an message embed containing up to five departures from the specified station.
     */
    private fun getEmbed(response: BoardResponse) = apiEmbedTemplate.andThen { spec ->
        spec.setTitle("Departures from ${response.getStation()}")
        response.elements.take(5).forEach { board ->
            spec.addField(
                board.getActualDateTime()?.getDateTimeString() ?: board.getPlannedDateTime().getDateTimeString(),
                "${board.product.line} to ${board.direction}",
                false
            )
        }
    }

    /**
     * Checks whether the count of args is correct and the last argument is an extId.
     */
    override fun isValidRequest(args: List<String>) = (args.size == 3) && isExtId(args[2])

    /**
     * Command usage for help message
     */
    override fun usage() = "$commandPrefix departures <station>"

}

package frank.api

import discord4j.common.util.Snowflake
import discord4j.core.`object`.entity.Message
import frank.api.response.TripResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Handles requests to the API.
 */
class Requester {

    /**
     * Cache to store previous trip requests for following trip detail requests.
     */
    private val tripResponseCache = HashMap<Snowflake, TripResponse>()

    /**
     * Queues a request to the API, calling [simpleCallback] on response.
     */
    fun <T> request(call: Call<T>, simpleCallback: SimpleCallback<T>) {
        call.enqueue(RequestCallback(simpleCallback))
    }

    /**
     * Caches a trip response with the associated [message].
     */
    fun cacheTripResponse(message: Message, response: TripResponse) = message.also {
        tripResponseCache[message.id] = response
    }

    /**
     * Checks whether a trip response is cached for the given [messageId].
     */
    fun hasTripResponse(messageId: Snowflake) = tripResponseCache.containsKey(messageId)

    /**
     * Retrieves the trip response matching the given [messageId].
     */
    fun getCachedTripResponse(messageId: Snowflake) = tripResponseCache.get(messageId)!!

    /**
     * Removes a cached trip response from cache.
     */
    fun forgetTripResponse(messageId: Snowflake) {
        tripResponseCache.remove(messageId)
    }
    //endregion

    /**
     * Simple callback interface with non-null type data.
     */
    fun interface SimpleCallback<T> {
        fun onResponse(data: T)
    }

    private class RequestCallback<T>(val simpleCallback: SimpleCallback<T>) : Callback<T> {

        override fun onResponse(call: Call<T>, response: Response<T>) {
            response.body()?.let { simpleCallback.onResponse(it) }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            t.printStackTrace()
        }

    }

}
